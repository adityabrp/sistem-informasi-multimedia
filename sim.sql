/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 5.6.21 : Database - sim
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sim` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sim`;

/*Table structure for table `tb_kue` */

DROP TABLE IF EXISTS `tb_kue`;

CREATE TABLE `tb_kue` (
  `id_kue` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kue` varchar(120) DEFAULT NULL,
  `keterangan` varchar(225) DEFAULT NULL,
  `kategori` varchar(60) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `stok` int(4) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_kue`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tb_kue` */

insert  into `tb_kue`(`id_kue`,`nama_kue`,`keterangan`,`kategori`,`harga`,`stok`,`gambar`) values 
(1,'Tart','ini kue Tart','Tart',60000,10,'tart1.jpg'),
(2,'Bolu Jogja','Ini Kue Bolu','Bolu',50000,5,'bolu.jpg'),
(3,'Gulung','Ini Kue Gulung','Gulung',50000,30,'gulung.jpg'),
(4,'Lapis','Ini Kue Lapis','Lapis',40000,20,'lapis.jpg');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

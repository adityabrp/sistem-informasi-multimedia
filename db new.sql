/*
SQLyog Professional v12.4.3 (64 bit)
MySQL - 10.1.36-MariaDB : Database - sim
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sim` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sim`;

/*Table structure for table `tb_kue` */

DROP TABLE IF EXISTS `tb_kue`;

CREATE TABLE `tb_kue` (
  `id_kue` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kue` varchar(120) DEFAULT NULL,
  `keterangan` varchar(225) DEFAULT NULL,
  `kategori` varchar(60) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `stok` int(4) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `video` varchar(50) DEFAULT NULL,
  `bahan` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id_kue`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tb_kue` */

insert  into `tb_kue`(`id_kue`,`nama_kue`,`keterangan`,`kategori`,`harga`,`stok`,`gambar`,`video`,`bahan`) values 
(1,'Tart','ini kue Tart','Tart',60000,10,'tart1.jpg','tart.mp4','6 butir telur, 220 gram gula halus, 250 gram tepung terigu, 250 gram mentega, 1/2 sdt baking powder, 1 sdm pasta coklat, Butter cream secukupnya, Coklat compound  secukupnya, Pewarna kue warna coklat'),
(2,'Bolu Jogja','Ini Kue Bolu','Bolu',50000,5,'bolu.jpg',NULL,NULL),
(3,'Gulung','Ini Kue Gulung','Gulung Pandan',50000,30,'gulung2.jpg','gulpan.mp4','4 butir telur, 120 gram gula pasir, 1/2 sdt SP, 75 gram terigu, 15 gram maizena, 27 gram susu bubuk, 1/4 sdt garam, 1/4 sdt vanili, 100 ml minyak goreng, 1 sdt pasta pandan, Buttercream'),
(4,'Lapis','Ini Kue Lapis','Lapis',40000,20,'lapis.jpg','lapisrainbow.mp4','500 gr tepung sagu tani, 250 gr tepung terigu, 1300 ml santan, 1/2 sdt garam, 500 gr gula, Vanili, Pandan');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

<br><br><div class="container-fluid">
<div>
<?php foreach($kue as $row) : ?>				
                            <ul class="nav nav-pills  justify-content-center"  role="tablist">
							<li class="nav-item">
								<a class="nav-link active" href="<?php echo base_url("dashboard/detail/" .$row->id_kue); ?>"  aria-selected="true">Produk Details</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url("dashboard/detail2/" .$row->id_kue); ?>" aria-selected="false">Intermezo</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url("dashboard/detail3/" .$row->id_kue); ?>" aria-selected="false">Testimoni</a>
							</li>
            </ul>
           
</div>
<br><br>
<div class="row shop-list single-pro-info no-sidebar">
<div class="col-lg-12">
							<div class="single-product clearfix">
								<!-- Single-pro-slider Big-photo start -->
								<div class="single-pro-slider single-big-photo view-lightbox slider-for">
									<div>
										<img src="img/single-product/medium/1.jpg" alt="" />
										<a class="view-full-screen" href="img/single-product/large/1.jpg"  data-lightbox="roadtrip" data-title="My caption">
											<i class="zmdi zmdi-zoom-in"></i>
										</a>
									</div>
									<div>
										<img src="img/single-product/medium/2.jpg" alt="" />
										<a class="view-full-screen" href="img/single-product/large/2.jpg"  data-lightbox="roadtrip" data-title="My caption">
											<i class="zmdi zmdi-zoom-in"></i>
										</a>
									</div>
									<div>
										<img src="img/single-product/medium/3.jpg" alt="" />
										<a class="view-full-screen" href="img/single-product/large/3.jpg"  data-lightbox="roadtrip" data-title="My caption">
											<i class="zmdi zmdi-zoom-in"></i>
										</a>
									</div>
									<div>
										<img src="img/single-product/medium/4.jpg" alt="" />
										<a class="view-full-screen" href="img/single-product/large/4.jpg"  data-lightbox="roadtrip" data-title="My caption">
											<i class="zmdi zmdi-zoom-in"></i>
										</a>
									</div>
									<div>
										<img src="img/single-product/medium/5.jpg" alt="" />
										<a class="view-full-screen" href="img/single-product/large/5.jpg"  data-lightbox="roadtrip" data-title="My caption">
											<i class="zmdi zmdi-zoom-in"></i>
										</a>
									</div>
								</div>	
								<!-- Single-pro-slider Big-photo end -->								
								<div class="product-info">
									<div class="fix">
										<h4 class="post-title floatleft">dummy Product name</h4>
										<span class="pro-rating floatright">
											<a href="#"><i class="zmdi zmdi-star"></i></a>
											<a href="#"><i class="zmdi zmdi-star"></i></a>
											<a href="#"><i class="zmdi zmdi-star"></i></a>
											<a href="#"><i class="zmdi zmdi-star-half"></i></a>
											<a href="#"><i class="zmdi zmdi-star-half"></i></a>
											<span>( 27 Rating )</span>
										</span>
									</div>
									<div class="fix mb-20">
										<span class="pro-price">$ 56.20</span>
									</div>
									<div class="product-description">
										<p>There are many variations of passages of Lorem Ipsum available, but the majority have be suffered alteration in some form, by injected humou or randomised words which donot look even slightly believable. If you are going to use a passage of Lorem Ipsum. </p>
									</div>
									<!-- color start -->								
									<div class="color-filter single-pro-color mb-20 clearfix">
										<ul>
											<li><span class="color-title text-capitalize">color</span></li>
											<li class="active"><a href="#"><span class="color color-1"></span></a></li>
											<li><a href="#"><span class="color color-2"></span></a></li>
											<li><a href="#"><span class="color color-7"></span></a></li>
											<li><a href="#"><span class="color color-3"></span></a></li>
											<li><a href="#"><span class="color color-4"></span></a></li>
										</ul>
									</div>
									<!-- color end -->
									<!-- Size start -->
									<div class="size-filter single-pro-size mb-35 clearfix">
										<ul>
											<li><span class="color-title text-capitalize">size</span></li>
											<li><a href="#">M</a></li>
											<li class="active"><a href="#">S</a></li>
											<li><a href="#">L</a></li>
											<li><a href="#">SL</a></li>
											<li><a href="#">XL</a></li>
										</ul>
									</div>
									<!-- Size end -->
									<div class="clearfix">
										<div class="cart-plus-minus">
											<input type="text" value="02" name="qtybutton" class="cart-plus-minus-box">
										</div>
										<div class="product-action clearfix">
											<a href="#" data-toggle="tooltip" data-placement="top" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
											<a href="#" data-toggle="modal"  data-target="#productModal" title="Quick View"><i class="zmdi zmdi-zoom-in"></i></a>
											<a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="zmdi zmdi-refresh"></i></a>
											<a href="#" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
										</div>
									</div>
									<!-- Single-pro-slider Small-photo start -->
									<div class="single-pro-slider single-sml-photo slider-nav">
										<div>
											<img src="img/single-product/small/1.jpg" alt="" />
										</div>
										<div>
											<img src="img/single-product/small/2.jpg" alt="" />
										</div>
										<div>
											<img src="img/single-product/small/3.jpg" alt="" />
										</div>
										<div>
											<img src="img/single-product/small/4.jpg" alt="" />
										</div>
										<div>
											<img src="img/single-product/small/5.jpg" alt="" />
										</div>
									</div>
									<!-- Single-pro-slider Small-photo end -->
								</div>
							</div>
						</div>
						<!-- Single-product end -->
					</div>
<?php endforeach; ?>


</div>


 
 <footer class="footer section section-sm">
  <!-- Container Start -->
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-7 offset-md-1 offset-lg-0">
        <!-- About -->
        <div class="block about">
          <!-- footer logo -->
          <!-- description -->
          <h3 class="alt-color" style="font-size=60px"><img height="40px" width="40px"alt="" 
          src="<?php echo base_url ('assets/img/logo.png" height="40px" width="40px"alt=""')?>" >CakeEat</h3>
        </div>
      </div>
      <!-- Link list -->
      <div class="col-lg-2 offset-lg-1 col-md-3">
        <div class="block">
          
        </div>
      </div>
      <!-- Link list -->
      <div class="col-lg-2 col-md-3 offset-md-1 offset-lg-0">
        <div class="block">
          
        </div>
      </div>
      <!-- Promotion -->
      <div class="col-lg-4 col-md-7">
        <!-- App promotion -->
        <div class="block-2 app-promotion">
          <a href="">
            <!-- Icon -->
            <img src="<?php echo base_url ('assets/images/footer/phone-icon.png" alt="mobile-icon"')?>">
          </a>
          <p>Get the CakeEat Mobile App and Save more</p>
        </div>
      </div>
    </div>
  </div>
  <!-- Container End -->
</footer>
<!-- Footer Bottom -->
<footer class="footer-bottom">
    <!-- Container Start -->
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-12">
          <!-- Copyright -->
          <div class="copyright">
            <p>Copyright © 2016. All Rights Reserved</p>
          </div>
        </div>
        <div class="col-sm-6 col-12">
          <!-- Social Icons -->
          <ul class="social-media-icons text-right">
              <li><a class="fa fa-facebook" href=""></a></li>
              <li><a class="fa fa-twitter" href=""></a></li>
              <li><a class="fa fa-pinterest-p" href=""></a></li>
              <li><a class="fa fa-vimeo" href=""></a></li>
            </ul>
        </div>
      </div>
    </div>
    <!-- Container End -->
    <!-- To Top -->
    <div class="top-to">
      <a id="top" class="" href=""><i class="fa fa-angle-up"></i></a>
    </div>
</footer>
 
 <!-- JAVASCRIPTS -->
 <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.')?>'"></script>
  <script src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js')?>"></script>
  <script src="<?php echo base_url('assets/plugins/tether/js/tether.min.js')?>"></script>
  <script src="<?php echo base_url('assets/plugins/raty/jquery.raty-fa.js')?>"></script>
  <script src="<?php echo base_url('assets/plugins/bootstrap/dist/js/popper.min.js')?>"></script>
  <script src="<?php echo base_url('assets/plugins/bootstrap/dist/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/plugins/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js')?>"></script>
  <script src="<?php echo base_url('assets/plugins/slick-carousel/slick/slick.min.js')?>"></script>
  <script src="<?php echo base_url('assets/plugins/jquery-nice-select/js/jquery.nice-select.min.js')?>"></script>
  <script src="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.pack.js')?>"></script>
  <script src="<?php echo base_url('assets/plugins/smoothscroll/SmoothScroll.min.js')?>"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>
  <script src="<?php echo base_url('assets/js/scripts.js')?>"></script>
  

</body>

</html>

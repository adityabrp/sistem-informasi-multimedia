<script src="<?php echo base_url('assets/aset/js/vendor/jquery-1.12.0.min.js')?>"></script>
		<!-- bootstrap js -->
		<script src="<?php echo base_url('assets/aset/js/bootstrap.min.js')?>"></script>
		<!-- jquery.meanmenu js -->
		<script src="<?php echo base_url('assets/aset/js/jquery.meanmenu.js')?>"></script>
		<!-- slick.min js -->
		<script src="<?php echo base_url('assets/aset/js/slick.min.js')?>"></script>
		<!-- jquery.treeview js -->
		<script src="<?php echo base_url('assets/aset/js/jquery.treeview.js')?>"></script>
		<!-- lightbox.min js -->
		<script src="<?php echo base_url('assets/aset/js/lightbox.min.js')?>"></script>
		<!-- jquery-ui js -->
		<script src="<?php echo base_url('assets/aset/js/jquery-ui.min.js')?>"></script>
		<!-- jquery.nivo.slider js -->
		<script src="<?php echo base_url('assets/aset/lib/js/jquery.nivo.slider.js')?>"></script>
		<script src="<?php echo base_url('assets/aset/lib/home.js')?>"></script>
		<!-- jquery.nicescroll.min js -->
		<script src="<?php echo base_url('assets/aset/js/jquery.nicescroll.min.js')?>"></script>
		<!-- countdon.min js -->
		<script src="<?php echo base_url('assets/aset/js/countdon.min.js')?>"></script>
		<!-- wow js -->
		<script src="<?php echo base_url('assets/aset/js/wow.min.js')?>"></script>
		<!-- plugins js -->
		<script src="<?php echo base_url('assets/aset/js/plugins.js')?>"></script>
		<!-- main js -->
		<script src="<?php echo base_url('assets/aset/js/main.js')?>"></script>
	</body>

<!-- Mirrored from preview.hasthemes.com/hurst-preview/hurst/single-product.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 27 Dec 2019 08:20:00 GMT -->
</html>
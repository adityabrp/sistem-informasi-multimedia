<html class="no-js" lang="en">
	
<!-- Mirrored from preview.hasthemes.com/hurst-preview/hurst/single-product.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 27 Dec 2019 08:18:41 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>CakeEat</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<!-- Place favicon.ico in the root directory -->
		
		<!-- Google Font -->
		<link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>

		<!-- all css here -->
		<!-- bootstrap v3.3.6 css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/css/bootstrap.min.css')?>">
		<!-- animate css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/css/animate.css')?>">
		<!-- jquery-ui.min css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/css/jquery-ui.min.css')?>">
		<!-- meanmenu css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/css/meanmenu.min.css')?>">
		<!-- nivo-slider css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/lib/css/nivo-slider.css')?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/lib/css/preview.css')?>">
		<!-- slick css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/css/slick.css')?>">
		<!-- lightbox css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/css/lightbox.min.css')?>">
		<!-- material-design-iconic-font css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/css/material-design-iconic-font.css')?>">
		<!-- All common css of theme -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/css/default.css')?>">
		<!-- style css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/style.css')?>">
        <!-- shortcode css -->
        <link rel="stylesheet" href="<?php echo base_url('assets/aset/css/shortcode.css')?>">
		<!-- responsive css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/aset/css/responsive.css')?>">
		<!-- modernizr css -->
		<script src="<?php echo base_url('assets/aset/js/vendor/modernizr-2.8.3.min.js')?>"></script>
	</head>
	<body>	
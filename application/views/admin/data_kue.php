<div class="container-fluid">
	<button class="btn btn-sm btn-primary mb-3" data-toggle = "modal" data-target="#tambah_kue">
		<i class="fas fa-plus fa-sm"></i>Tambah Kue
	</button>

	<table class="table table-bordered">
		<tr>
			<th>No</th>
			<th>Nama Kue</th>
			<th>Keterangan</th>
			<th>Kategori</th>
			<th>Harga</th>
			<th>Stok</th>
            <th>Bahan</th>
			<th colspan="3">Aksi</th>
		</tr>

		<?php
		$no=1;
		foreach ($kue as $row) : ?>

			<tr>
				<td><?php echo $no++ ?></td>
				<td><?php echo $row->nama_kue ?></td>
				<td><?php echo $row->keterangan ?></td>
				<td><?php echo $row->kategori ?></td>
				<td><?php echo $row->harga ?></td>
				<td><?php echo $row->stok ?></td>
                <td><?php echo $row->bahan ?></td>
				<td><div class="btn btn-success btn-sm"><i class="fas fa-search-plus"></i></div></td>
				<td><div class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></div></td>
				<td><div class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></div></td>
			</tr>

		<?php endforeach; ?>
	</table>
</div>

<div class="modal fade" id="tambah_kue" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Kue</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(). 'admin/data_kue/tambah_kue'; ?>" method = "post" enctype="multipart/form-data">
        	<div class="form-group">
        		<label>Nama Kue</label>
        		<input type="text" name="nama_kue" class="form-control">
        	</div>
        	<div class="form-group">
        		<label>Keterangan</label>
        		<input type="text" name="keterangan" class="form-control">
        	</div>
        	<div class="form-group">
                <label>Kategori</label>
                    <select name="kategori" class="form-control">
                        <option value="">--Select Kategori--</option>
                        <option value="lapis">Lapis</option>
                        <option value="tart">Tart</option>
                        <option value="bolu">Bolu</option>
                        <option value="gulung">Gulung</option>
                    </select>
                    <span class="help-block"></span>
            </div>
        	<div class="form-group">
        		<label>Harga</label>
        		<input type="text" name="harga" class="form-control">
        	</div>
        	<div class="form-group">
        		<label>Stok</label>
        		<input type="text" name="stok" class="form-control">
        	</div>
            <div class="form-group">
                <label>Bahan</label>
                <input type="text" name="bahan" class="form-control">
            </div>
        	<div class="form-group">
        		<label>Gambar Kue</label>
        		<input type="file" name="gambar" class="form-control">
        	</div>
            <div class="form-group">
                <label>Video</label>
                <input type="file" name="video" class="form-control">
            </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Tambah Data</button>
	      </div>
	    </form>
    </div>
  </div>
</div>
<style>

header {
  position: relative;
  background-color: black;
  height: 75vh;
  min-height: 25rem;
  width: 100%;
  overflow: hidden;
}
header video {
  position: absolute;
  top: 50%;
  left: 50%;
  min-width: 100%;
  min-height: 100%;
  width: 100%;
  height: 100%;
  z-index: 0;
  -ms-transform: translateX(-50%) translateY(-50%);
  -moz-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translateX(-50%) translateY(-50%);
  transform: translateX(-50%) translateY(-50%);
}

header .overlay {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: black;
  opacity: 0.5;
  z-index: 1;
}</style>
<br><br>
<div class="container-fluid">
<div>
<?php foreach($kue as $row) : ?>				
                            <ul class="nav nav-pills  justify-content-center"  role="tablist">
							<li class="nav-item">
								<a class="nav-link " href="<?php echo base_url("dashboard/detail/" .$row->id_kue); ?>"  aria-selected="true">Produk Details</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url("dashboard/detail2/" .$row->id_kue); ?>" aria-selected="false">Intermezo</a>
							</li>
							<li class="nav-item">
								<a class="nav-link active" href="<?php echo base_url("dashboard/detail3/" .$row->id_kue); ?>" aria-selected="false">Testimoni</a>
							</li>
            </ul>
           
</div>
<br><br>
<header>
                      
                      <video playsinline="playsinline" autoplay="autoplay" muted="no"  loop="loop" height="100%">
                        <source src="<?php echo base_url().'assets/video/'.$row->v_harga ?>" type="video/mp4">
                      </video>
            </header>
<br>

            <ul class="nav nav-pills  justify-content-center"  role="tablist">
            <li class="nav-item">
              <a class="nav-link " href="<?php echo base_url("dashboard/detail3/" .$row->id_kue); ?>"  aria-selected="true">Rasa</a>
							</li>
							<li class="nav-item">
              <a class="nav-link " href="<?php echo base_url("dashboard/detail4/" .$row->id_kue); ?>"  aria-selected="true">Tampilan</a>
							</li>
							<li class="nav-item">
              <a class="nav-link active" href="<?php echo base_url("dashboard/detail5/" .$row->id_kue); ?>"  aria-selected="true">Harga</a>
							</li>
            </ul>
<?php endforeach; ?>
</div>
<br><br>
<?php

class Tart_custom extends CI_Controller {
	public function __construct (){
		parent::__construct();
		$this->load->model("model_customtart") ;
	}
    public function index()
    {
    	$data['kue'] = $this->model_customtart->tampil_data()->result();
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('tart_custom', $data);
        $this->load->view('template/footer');
    }
}
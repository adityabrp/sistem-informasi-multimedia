<?php

class Bolu extends CI_Controller {
	public function __construct (){
		parent::__construct();
		$this->load->model("model_bolu") ;
	}
    public function index()
    {
    	$data['kue'] = $this->model_bolu->tampil_data()->result();
        $this->load->view('template/headd');
        $this->load->view('bolu', $data);
        $this->load->view('template/foott');
    }
}
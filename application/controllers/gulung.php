<?php

class Gulung extends CI_Controller {
	public function __construct (){
		parent::__construct();
		$this->load->model("model_gulung") ;
	}
    public function index()
    {
    	$data['kue'] = $this->model_gulung->tampil_data()->result();
        $this->load->view('template/headd');
        $this->load->view('gulung', $data);
        $this->load->view('template/foott');
    }
}
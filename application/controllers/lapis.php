<?php

class Lapis extends CI_Controller {
	public function __construct (){
		parent::__construct();
		$this->load->model("model_lapis") ;
	}
    public function index()
    {
    	$data['kue'] = $this->model_lapis->tampil_data()->result();
        $this->load->view('template/headd');
        $this->load->view('lapis', $data);
        $this->load->view('template/foott');
    }
}
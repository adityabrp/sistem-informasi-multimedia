<?php

 class Data_kue extends CI_Controller
 {
 	function __construct(){
		parent::__construct();
		  $this->load->helper(array('form', 'url'));
	}

 	public function index() {
 		$data['kue'] = $this->model_kue->tampil_data()->result();
 		$this->load->view('template_admin/header');
 		$this->load->view('template_admin/sidebar');
 		$this->load->view('admin/data_kue', $data);
 		$this->load->view('template_admin/footer');
 	}

 	public function tambah_kue() {
 		$nama_kue = $this->input->post('nama_kue');
 		$keterangan = $this->input->post('keterangan');
 		$kategori = $this->input->post('kategori');
 		$harga = $this->input->post('harga');
 		$stok = $this->input->post('stok');
 		$gambar = $this->upload_image();
        $video = $this->upload_video();

 		$data = array (
 			'nama_kue'	=> $nama_kue,
 			'keterangan'	=> $keterangan,
 			'kategori'	=> $kategori,
 			'harga'	=> $harga,
 			'stok'	=> $stok,
 			'gambar'	=> $gambar,
            'video'    => $video
 		);

 		$this->model_kue->tambah_kue($data, 'tb_kue');
 		redirect('admin/data_kue/index');
 	}

 	function upload_image(){
        $config['upload_path'] = './assets/gambar_kue/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
 
        $this->load->library('upload',$config);
        if(!empty($_FILES['gambar']['name'])){
                if(!$this->upload->do_upload('gambar'))
                    $this->upload->display_errors();  
                else
                    echo "Foto berhasil di upload";
        }        
    }

    function upload_video(){
        $config['upload_path'] = './assets/video/'; //path folder
        $config['allowed_types'] = 'mp4|3gp|mkv'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
 
        $this->load->library('upload',$config);
        if(!empty($_FILES['video']['name'])){
                if(!$this->upload->do_upload('video'))
                    $this->upload->display_errors();  
                else
                    echo "Video berhasil di upload";
        }        
    }
 }
?>
<?php

class Dashboard extends CI_Controller {
     function __construct (){
		parent::__construct();
        $this->load->model("model_kue") ;
        $this->load->helper(array('url'));
	}
    public function index()
    {
    	$data['kue'] = $this->model_kue->tampil_data()->result();
        $this->load->view('template/headd');
        $this->load->view('dashboard', $data);
        $this->load->view('template/foott');
    }

    public function detail()
    {
        $id = $this->uri->segment(3);
    	$data['kue'] = $this->model_kue->detail($id);
        $this->load->view('template/headd');
        $this->load->view('detail', $data);
        $this->load->view('tabel',$data);
        $this->load->view('template/foott');
    }

    public function detail2()
    {
        $id = $this->uri->segment(3);
    	$data['kue'] = $this->model_kue->detail2($id);
        $this->load->view('template/headd');
        $this->load->view('detail', $data);
        $this->load->view('tutorial',$data);
        $this->load->view('template/foott');
    }

    public function detail3()
    {
        $id = $this->uri->segment(3);
    	$data['kue'] = $this->model_kue->detail3($id);
        $this->load->view('template/headd');
        $this->load->view('detail', $data);
        $this->load->view('testimoni',$data);
        $this->load->view('template/foott');
    }

    public function detail4()
    {
        $id = $this->uri->segment(3);
    	$data['kue'] = $this->model_kue->detail3($id);
        $this->load->view('template/headd');
        $this->load->view('detail', $data);
        $this->load->view('testimoni2',$data);
        $this->load->view('template/foott');
    }

    public function detail5()
    {
        $id = $this->uri->segment(3);
    	$data['kue'] = $this->model_kue->detail3($id);
        $this->load->view('template/headd');
        $this->load->view('detail', $data);
        $this->load->view('testimoni3',$data);
        $this->load->view('template/foott');
    }
}
?>
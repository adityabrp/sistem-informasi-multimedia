<?php

class Tart extends CI_Controller {
	public function __construct (){
		parent::__construct();
		$this->load->model("model_tart") ;
	}
    public function index()
    {
    	$data['kue'] = $this->model_tart->tampil_data()->result();
        $this->load->view('template/headd');
        $this->load->view('tart', $data);
        $this->load->view('template/foott');
    }
}